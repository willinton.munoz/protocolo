import socket
import tqdm
import os


class Main:
    def __init__(self):
        self.SEPARATOR = "<SEPARATOR>"
        self.BUFFER_SIZE = 4096
        self.host = "localhost"
        self.port = 5001
        self.menu()

    def menu(self):
        syscontrol = True
        while syscontrol:
            print("································")
            print("1. Bucket \n"
                  "2. Archivos \n"
                  "3. Salir\n")
            response = int(input(".: Selecciona una opción: "))

            if response == 1:
                self.bucket_menu()
            elif response == 2:
                self.files_menu()
            elif response == 3:
                syscontrol = False

    def connect(self):
        print("------------------------")
        s = socket.socket()
        print(f'[+] Connecting to {self.host}:{self.port}')
        s.connect((self.host, self.port))
        print("[+] Connected")
        print("------------------------")
        return s
    
    def bucket_menu(self):
        print("································")
        print("1. Añadir bucket\n"
              "2. Borrar bucket\n"
              "3. Listar buckets\n"
              "4. Cambiar nombre\n"
              "5. Volver\n")
        response = int(input(".: Selecciona una opción: "))
        
        if response == 1:
            bucket_name = input("[INPUT] Ingresa el nombre del bucket: ")
            s = self.connect()
            s.send(f"bucket{self.SEPARATOR}add{self.SEPARATOR}{bucket_name}".encode())
            self.process_response(s.recv(self.BUFFER_SIZE).decode())
            s.close()

        elif response == 2:
            bucket_name = input("[INPUT] Ingresa el nombre del bucket: ")
            confirmation = input("[INFO] ¿Estas seguro que deseas eliminar el bucket y sus archivos? (yes, no): ").lower()
            if confirmation == "yes":
                s = self.connect()
                s.send(f"bucket{self.SEPARATOR}delete{self.SEPARATOR}{bucket_name}".encode())
                self.process_response(s.recv(self.BUFFER_SIZE).decode())
                s.close()

        elif response == 3:
            s = self.connect()
            s.send(f"bucket{self.SEPARATOR}list".encode())
            self.process_response(s.recv(self.BUFFER_SIZE).decode())
            s.close()

        elif response == 4:
            old_name = input("[INPUT] Ingresa el nombre del bucket: ")
            new_name = input("[INPUT] Ingresa el nuevo nombre: ")
            s = self.connect()
            s.send(f"bucket{self.SEPARATOR}update_name".encode())
            if s.recv(self.BUFFER_SIZE).decode() == "received":
                s.send(f"{old_name}{self.SEPARATOR}{new_name}".encode())
            self.process_response(s.recv(self.BUFFER_SIZE).decode())
            s.close()

        elif response == 5:
            pass

        else:
            print("[INFO] La opción no existe")

    def process_response(self, response):
        response = response.split(self.SEPARATOR)
        if response[1] == "error":
            print(f"\nError en la transacción {response[0]} error: {response[2]}\n")
        elif response[1] == "list":
            print(f"\n{'Buckets' if response[0] == 'bucket' else 'Archivos'} Disponibles:")
            datos = response[2].split(",")
            for dato in datos:
                print(f"- {dato}")
            print("\n")
        else:
            print(f'\nTransacción terminada correctamente para {response[0]} con resultado: {response[1]}\n')

    def files_menu(self):
        print("································")
        print("1. Añadir archivo\n"
              "2. Listar archivos\n"
              "3. Borrar archivo\n"
              "4. Cambiar nombre a un archivo\n"
              "5. Descargar archivo\n"
              "6. Volver\n")
        response = int(input(".: Selecciona una opción: "))
        
        if response == 1:
            filename = input("[INPUT] Dirección del archivo: ")
            bucket = input("[INPUT] Bucket de destino: ")
            filesize = os.path.getsize(filename)
            s = self.connect()
            s.send(f"file{self.SEPARATOR}add".encode())
            if s.recv(self.BUFFER_SIZE).decode() == "received":
                s.send(f"{filename}{self.SEPARATOR}{filesize}{self.SEPARATOR}{bucket}".encode())
                if s.recv(self.BUFFER_SIZE).decode() == "received":
                    progress = tqdm.tqdm(range(filesize), f'Sending {filename}', unit='B', unit_scale=True,
                                         unit_divisor=1024)
                    try:
                        with open(filename, "rb") as f:
                            for _ in progress:
                                bytes_read = f.read(self.BUFFER_SIZE)
                                if not bytes_read:
                                    break

                                s.sendall(bytes_read)
                                progress.update(len(bytes_read))
                        print("File sended")
                    except BrokenPipeError as error:
                        print("\n Error enviando el archivo \n")
                else:
                    print("\nA ocurrido alguún error al enviar el archivo. Probablemente el archivo ya exista en el repositorio\n")
            s.close()

        elif response == 2:
            bucket = input("[INPUT] Ingresa el nombre del bucket a listar: ")
            s = self.connect()
            s.send(f"file{self.SEPARATOR}ls".encode())
            if s.recv(self.BUFFER_SIZE).decode() == "received":
                s.send(bucket.encode())
                self.process_response(s.recv(self.BUFFER_SIZE).decode())
            s.close()

        elif response == 3:
            bucket = input("[INPUT] Ingresa el nombre del bucket donde esta el archivo: ")
            file = input("[INPUT] Ingresa el nombre del archivo: ")
            s = self.connect()
            s.send(f"file{self.SEPARATOR}rm{self.SEPARATOR}{bucket}".encode())
            if s.recv(self.BUFFER_SIZE).decode() == "received":
                s.send(file.encode())
                self.process_response(s.recv(self.BUFFER_SIZE).decode())
            s.close()

        elif response == 4:
            bucket = input("[INPUT] Ingresa el nombre del bucket donde esta el archivo: ")
            old = input("[INPUT] Ingresa el nombre del archivo: ")
            new = input("[INPUT] Ingresa el nuevo nombre del archivo: ")
            s = self.connect()
            s.send(f"file{self.SEPARATOR}rename{self.SEPARATOR}{bucket}".encode())
            if s.recv(self.BUFFER_SIZE).decode() == "received":
                s.send(f"{old}{self.SEPARATOR}{new}".encode())
                self.process_response(s.recv(self.BUFFER_SIZE).decode())
            s.close()

        elif response == 5:
            bucket = input("[INPUT] Ingresa el nombre del bucket donde esta el archivo: ")
            file_name = input("[INPUT] Ingresa el nombre del archivo: ")
            save_url = input("[INPUT] Ingresa la dirección de guardado: ")
            
            s = self.connect()
            s.send(f"file{self.SEPARATOR}download{self.SEPARATOR}{bucket}".encode())
            if s.recv(self.BUFFER_SIZE).decode() == "received":
                s.send(file_name.encode())

            filename, filesize = s.recv(self.BUFFER_SIZE).decode().split(self.SEPARATOR)

            filesize = int(filesize)

            progress = tqdm.tqdm(range(filesize), f'Receiving {filename}', unit='B',
                                 unit_scale=True, unit_divisor=1024, leave=False)

            wait = True

            while wait:
                try:
                    with open(f"{save_url}/{filename}", "wb") as f:
                        for _ in progress:
                            bytes_read = s.recv(self.BUFFER_SIZE)
                            if not bytes_read:
                                wait = False
                                break
                            f.write(bytes_read)
                            progress.update(len(bytes_read))

                    print("File received and save")

                except Exception as err:
                    print("Error receiving the file", err)

            s.close()

        elif response == 6:
            pass


if __name__ == '__main__':
    Main()
