import { Injectable } from "@angular/core";
import { Subject } from 'rxjs';


@Injectable()
export class WebsocketService {
    public socket: WebSocket;
    public sender = new Subject<MessageEvent>();

    constructor() { }

    public sendMessage(code: codes, body: any) {
        var message: messageStandard = {
            code: code,
            body: body
        }
        try {
            this.socket.send(JSON.stringify(message));
        } catch(error) {
            console.error(error);
        }
    }

    public connect() {
        this.socket = new WebSocket("ws://localhost:4041/");
        this.socket.binaryType = 'arraybuffer';
        this.socket.onopen = () => {
            this.socket.send(JSON.stringify({
                code: "connect"
            }));
        }

        this.socket.onmessage = (message: MessageEvent) => {
            this.sender.next(message);
        }

        this.socket.onerror = (error) => {
            console.error(error);
        }

        this.socket.onclose = (ev) => {
            console.log(ev)
            console.log("Connection closed");
        }

        return this.sender.asObservable();
    }
}

interface messageStandard {
    code: string,
    body?: any | any[]
}

export enum codes {
    connection = "connect",
    create_bucket = "bucket_create",
    list_buckets = "bucket_ls",
    delete_bucket = "bucket_delete",
    add_file = "file_add",
    list_files = "file_ls",
    file_download = "file_download",
    delete_file = "file_delete"
}