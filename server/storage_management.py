import os
import json
from localStoragePy import localStoragePy
from hash_generator import HashGenerator
import shutil


class StorageManagement:
    __storage = None
    __hash = HashGenerator()
    __separator = "<SEPARATOR>"

    def __init__(self):
        self.__storage = localStoragePy('descriptor')
        f = open('descriptor', 'r')
        self.__storage.setItem('descriptor', f.read())

    def __storage_to_prevent_loose(self):
        f = open('descriptor', 'w')
        f.write(self.__storage.getItem('descriptor'))
        f.close()

    def create_bucket(self, bucket_name):
        buckets = json.loads(self.__storage.getItem("descriptor"))
        exits = False
        for bucket in buckets.get("buckets_names"):
            if bucket == bucket_name:
                exits = True

        if exits:
            return f"error{self.__separator}El bucket ya existe"

        os.mkdir('buckets/'+bucket_name)

        buckets["buckets_names"].append(bucket_name),
        buckets["buckets"][bucket_name] = []

        self.__storage.setItem("descriptor", json.dumps(buckets))
        self.__storage_to_prevent_loose()

        return f"Bucket {bucket_name} creado correctamente"

    def list_buckets(self):
        buckets = json.loads(self.__storage.getItem("descriptor"))
        buckets_names = ""
        for bucket in buckets.get("buckets_names"):
            buckets_names += f"{bucket},"
        buckets_names = buckets_names[:len(buckets_names)-1]

        return buckets_names

    def rm_bucket(self, name):
        buckets = json.loads(self.__storage.getItem('descriptor'))
        names = buckets.get('buckets_names')
        index = -1
        for i, bucket in enumerate(names):
            if bucket == name:
                index = i

        if index == -1:
            return f"error{self.__separator}El buckect no existe"

        shutil.rmtree('buckets/'+name)

        buckets["buckets_names"].remove(name)
        del buckets["buckets"][name]
        self.__storage.setItem("descriptor", json.dumps(buckets))
        self.__storage_to_prevent_loose()
        return f"Bucket {name} borrado correctamente"

    def rename_bucket(self, old_name, new_name):
        buckets = json.loads(self.__storage.getItem('descriptor'))
        names = buckets.get('buckets_names')
        index = -1
        for i, bucket in enumerate(names):
            if bucket == old_name:
                index = i

        if index == -1:
            return f"error{self.__separator}El buckect no existe"

        buckets["buckets_names"].remove(old_name)
        buckets["buckets_names"].append(new_name)
        buckets["buckets"][new_name] = buckets["buckets"].pop(old_name)
        os.rename(f"buckets/{old_name}", f"buckets/{new_name}")
        self.__storage.setItem("descriptor", json.dumps(buckets))
        self.__storage_to_prevent_loose()
        return f"Bucket name change to {new_name}"

    def add_file(self, bucket, file_name, client):
        buckets = json.loads(self.__storage.getItem('descriptor'))
        names = buckets.get('buckets_names')
        index = -1
        for i, _bucket in enumerate(names):
            if _bucket == bucket:
                index = i

        if index == -1:
            return True, f"error{self.__separator}El bucket no existe"

        index = -1

        for i, file in enumerate(buckets['buckets'][bucket]):
            if file["fileName"] == file_name:
                index = i

        if index > -1:
            print("El archivo ya existe")
            return True, f"error{self.__separator}El archivo ya existe"

        buckets['buckets'][bucket].append({
            "fileName": file_name,
            "fileHash": self.__hash.generate_hash(),
            "url": f"buckets/{bucket}/{file_name}"
        })

        self.__storage.setItem("descriptor", json.dumps(buckets))
        self.__storage_to_prevent_loose()

        return False, "Archivo guardado correctamente"

    def ls_file(self, bucket):
        buckets = json.loads(self.__storage.getItem('descriptor'))
        names = buckets.get('buckets_names')
        index = -1
        for i, _bucket in enumerate(names):
            if _bucket == bucket:
                index = i

        if index == -1:
            return f"error{self.__separator}El bucket no existe"

        files = ""
        for file in buckets['buckets'][bucket]:
            files += f"{file['fileName']} - {file['fileHash']},"

        return files[:len(files)-1]

    def remove_file(self, bucket, file_name):
        buckets = json.loads(self.__storage.getItem('descriptor'))
        names = buckets.get('buckets_names')
        index = -1
        for i, _bucket in enumerate(names):
            if _bucket == bucket:
                index = i

        if index == -1:
            return f"error{self.__separator}El bucket no existe"

        file = None

        for i, _file in enumerate(buckets['buckets'][bucket]):
            if _file["fileName"] == file_name:
                file = _file

        if file is None:
            return f"error{self.__separator}El archivo no existe"

        os.remove(f"buckets/{bucket}/{file_name}")
        buckets['buckets'][bucket].remove(file)
        self.__storage.setItem("descriptor", json.dumps(buckets))
        self.__storage_to_prevent_loose()
        return f"Archivo {file_name} borrado correctamente"

    def rename(self, old, new, bucket):
        buckets = json.loads(self.__storage.getItem('descriptor'))
        names = buckets.get('buckets_names')
        index = -1
        for i, _bucket in enumerate(names):
            if _bucket == bucket:
                index = i

        if index == -1:
            return f"error{self.__separator}El bucket no existe"

        file = None

        for i, _file in enumerate(buckets['buckets'][bucket]):
            if _file["fileName"] == old:
                _file["fileName"] = new
                _file["url"] = f"buckets/{bucket}/{new}"
                file = _file

        if file is None:
            return f"error{self.__separator}El archivo no existe"

        os.rename(f"buckets/{bucket}/{old}", f"buckets/{bucket}/{new}")
        self.__storage.setItem("descriptor", json.dumps(buckets))
        self.__storage_to_prevent_loose()
        return f"Nombre del archivo cambiado"

    def get_file(self, file_name, bucket):
        buckets = json.loads(self.__storage.getItem('descriptor'))
        names = buckets.get('buckets_names')
        index = -1
        for i, _bucket in enumerate(names):
            if _bucket == bucket:
                index = i

        if index == -1:
            return True, f"error{self.__separator}El bucket no existe"

        file = None

        for i, _file in enumerate(buckets['buckets'][bucket]):
            if _file["fileName"] == file_name:
                file = _file

        if file is None:
            return True, f"error{self.__separator}El archivo no existe"

        return False, file
