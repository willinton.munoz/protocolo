import socket
import threading
from engine import Engine


class Main:
    running = True
    engine = Engine()

    def __init__(self):
        self.SERVER_HOST = "0.0.0.0"
        self.SERVER_PORT = 5001
        self.BUFFER_SIZE = 4096
        self.SEPARATOR = "<SEPARATOR>"

        self.s = socket.socket()
        self.s.bind((self.SERVER_HOST, self.SERVER_PORT))

        self.s.listen(5)
        print(f"[*] Listening as {self.SERVER_HOST}:{self.SERVER_PORT}")

        while self.running:
            client_socket, address = self.s.accept()
            print(f"[+] {address} is connected. ")

            threading.Thread(target=self.client, args=(client_socket, address)).start()

        self.s.close()

    def client(self, client_socket, addr):
        response = self.engine.define_action(client_socket, addr)
        client_socket.send(response.encode())

        client_socket.close()
        print("·····························")


if __name__ == '__main__':
    Main()
