import random
import hashlib
import time


class HashGenerator:
    __instance = None

    def __new__(cls):
        if not isinstance(cls.__instance, cls):
            cls.__instance = object.__new__(cls)
        return cls.__instance

    def generate_hash(self):
        hash = hashlib.sha1()
        hash.update(str(time.time()).encode())
        return hash.hexdigest()[:32]

