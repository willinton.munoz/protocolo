from storage_management import StorageManagement
import os
import tqdm


class Engine:
    instance = None
    SEPARATOR = "<SEPARATOR>"
    storage = StorageManagement()

    def __new__(cls):
        if hasattr(cls, 'instance'):
            cls.instance = object.__new__(cls)
        return cls.instance

    def define_action(self, client, addr):
        message = client.recv(4096).decode()
        message_splited = message.split(self.SEPARATOR)

        if len(message_splited) > 2:
            identifier, action, bucket = message_splited
            print(identifier, action, bucket)
            if identifier == "bucket":
                if action == "add":
                    return f"bucket{self.SEPARATOR}{self.storage.create_bucket(bucket)}"

                elif action == "delete":
                    return f"bucket{self.SEPARATOR}{self.storage.rm_bucket(bucket)}"

                else:
                    print("Action not exists")

            elif identifier == "file":
                if action == "rm":
                    client.send("received".encode())
                    file = client.recv(4096).decode()
                    return f"file{self.SEPARATOR}{self.storage.remove_file(bucket, file)}"
                elif action == "download":
                    client.send("received".encode())
                    file = client.recv(4096).decode()

                    error, file_information = self.storage.get_file(file, bucket)

                    if error:
                        return f"file{self.SEPARATOR}error{self.SEPARATOR}{file_information}"

                    file_size = os.path.getsize(file_information["url"])
                    file_size = int(file_size)
                    client.send(f"{file_information['fileName']}{self.SEPARATOR}{file_size}".encode())
                    progress = tqdm.tqdm(range(file_size), f'Sending {file_information["fileName"]}', unit='B', unit_scale=True,
                                         unit_divisor=1024)

                    try:
                        with open(file_information["url"], "rb") as f:
                            for _ in progress:
                                bytes_read = f.read(4096)
                                if not bytes_read:
                                    break
                                client.send(bytes_read)
                                progress.update(len(bytes_read))
                        print("File sent")
                    except Exception as err:
                        client.close()
                        print("Error enviando el archivo", err)

                    return f"download{self.SEPARATOR}Archivo entregado"

                elif action == "rename":
                    client.send("received".encode())
                    old, new = client.recv(4096).decode().split(self.SEPARATOR)
                    return f"file{self.SEPARATOR}{self.storage.rename(old, new, bucket)}"

                else:
                    print("Action not exists")

        else:
            identifier, action = message_splited
            print(identifier, action)
            if identifier == "bucket":
                if action == "list":
                    self.storage.list_buckets()
                    return f"bucket{self.SEPARATOR}list{self.SEPARATOR}{self.storage.list_buckets()}"
                elif action == "update_name":
                    client.send("received".encode())
                    names = client.recv(4096).decode().split(self.SEPARATOR)
                    return f"bucket{self.SEPARATOR}{self.storage.rename_bucket(names[0], names[1])}"
                else:
                    print("Action not exists")

            elif identifier == "file":
                if action == "add":
                    client.send("received".encode())
                    filename, filesize, bucket = client.recv(4096).decode().split(self.SEPARATOR)
                    filename = os.path.basename(filename)
                    filesize = int(filesize)
                    error, response = self.storage.add_file(bucket, filename, client)

                    if error:
                        return f"file{self.SEPARATOR}{response}"

                    client.send("received".encode())

                    progress = tqdm.tqdm(range(filesize), f'Receiving from {addr[0]} {filename}', unit='B',
                                         unit_scale=True, unit_divisor=1024, leave=False)
                    wait = True
                    while wait:
                        try:
                            with open(f"buckets/{bucket}/{filename}", "wb") as f:
                                for _ in progress:
                                    bytes_read = client.recv(4096)
                                    if not bytes_read:
                                        wait = False
                                        break
                                    f.write(bytes_read)
                                    progress.update(len(bytes_read))
                        except FileNotFoundError as error:
                            client.close()
                    return f"file{self.SEPARATOR}{response}"

                elif action == "ls":
                    client.send("received".encode())
                    bucket_ls = client.recv(4096).decode()

                    return f"file{self.SEPARATOR}list{self.SEPARATOR}{self.storage.ls_file(bucket_ls)}"

                else:
                    print("Action doesn't exists")

            else:
                print("Action doesn't exists")
